package pers.xdrodger.springboot.demo.entity;

import java.util.Date;

/**
 * Created by xdrodger on 04/08/2018.
 */
public class AccessLog {
    private Long id;
    private String ip;
    private String url;
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
