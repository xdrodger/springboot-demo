package pers.xdrodger.springboot.demo.repo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**
 * Created by xdrodger on 04/08/2018.
 */
@Configuration
public class DataSourceConfig {
    @Bean(name = "ds")
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource.db1") // application-datasource.properteis中对应属性的前缀
    public DataSource dataSource1() {
        return DataSourceBuilder.create().build();
    }
}
