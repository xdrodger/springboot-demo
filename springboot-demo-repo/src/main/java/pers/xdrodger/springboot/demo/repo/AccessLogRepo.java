package pers.xdrodger.springboot.demo.repo;

import pers.xdrodger.springboot.demo.entity.AccessLog;

/**
 * Created by xdrodger on 04/08/2018.
 */
public interface AccessLogRepo {
    AccessLog findById(Long id);
}
