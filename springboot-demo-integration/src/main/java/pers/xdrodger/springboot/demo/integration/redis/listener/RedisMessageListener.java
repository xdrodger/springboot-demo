package pers.xdrodger.springboot.demo.integration.redis.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Component;

@Component
public class RedisMessageListener implements MessageListener {

	@Autowired
	private StringRedisTemplate redisTemplate;
	
	@Override
	public void onMessage(Message message, byte[] pattern) {
		RedisSerializer<?> serializer = redisTemplate.getStringSerializer();
		String messageStr = (String) serializer.deserialize(message.getBody());
		String topic =  (String) serializer.deserialize(message.getChannel());
		System.out.println("messageStr：" + messageStr);
		System.out.println("topic：" + topic);
		System.out.println("pattern：" + redisTemplate.getStringSerializer().deserialize(pattern));
	}

}
