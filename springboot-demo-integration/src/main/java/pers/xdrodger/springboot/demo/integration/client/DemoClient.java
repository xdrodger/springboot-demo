package pers.xdrodger.springboot.demo.integration.client;

import pers.xdrodger.springboot.demo.entity.AccessLog;

/**
 * Created by xdrodger on 04/08/2018.
 */
public interface DemoClient {
    AccessLog findById(Long id);

    boolean testExcel();

    boolean testRedis();
}
