package pers.xdrodger.springboot.demo.integration.redis.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import redis.clients.jedis.JedisPubSub;

/**
 * Created by xdrodger on 02/02/2018.
 */
@Component
public class PubSubListener extends JedisPubSub {

    private static final Logger LOGGER = LoggerFactory.getLogger(PubSubListener.class);

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
    public void onMessage(String message, String channel) {
        LOGGER.info("Received <{}> from channel <{}>", message, channel);
    }

    @Override
    public void onSubscribe(String channel, int subscribedChannels) {
        LOGGER.info("订阅渠道" + channel);
    }

    @Override
    public void onUnsubscribe(String channel, int subscribedChannels) {
        LOGGER.info("取消订阅渠道" + channel);
    }
}
