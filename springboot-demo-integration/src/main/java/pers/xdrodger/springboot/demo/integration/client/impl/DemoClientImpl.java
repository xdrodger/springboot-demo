package pers.xdrodger.springboot.demo.integration.client.impl;

import com.zg.framework.utils.BeanMapper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import pers.xdrodger.springboot.demo.entity.AccessLog;
import pers.xdrodger.springboot.demo.integration.client.DemoClient;
import pers.xdrodger.springboot.demo.service.AccessLogService;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by xdrodger on 04/08/2018.
 */
@Component
public class DemoClientImpl implements DemoClient {
    @Autowired
    private AccessLogService accessLogService;
    @Autowired
    private RedisTemplate redisTemplate;

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public AccessLog findById(Long id) {
        return accessLogService.findById(id);
    }

    @Override
    public boolean testExcel() {
        List<String> dataList = new ArrayList<>();
        dataList.add("demo");

        dataList.add(sdf.format(new Date()));
        appendToDemoExcel(dataList);

        return true;
    }

    @Override
    public boolean testRedis() {
        redisTemplate.opsForValue().set("test:set", sdf.format(new Date()));
        return true;
    }

    private boolean appendToDemoExcel(List<String> dataList) {
        String filePath = "./" + "demo.xlsx";
        File file = new File(filePath);
        if(!file.exists()){
            createDemoExcel(filePath, dataList);
        } else {
            try {
                appendToExcel(filePath, dataList);
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    private boolean createDemoExcel(String filePath, List<String> dataList) {
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet sheet = wb.createSheet("Sheet1");
        // 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制short
        XSSFRow row = sheet.createRow(0);
        // 第四步，创建单元格，并设置值表头 设置表头居中
        XSSFCellStyle style = wb.createCellStyle();
        Font font = wb.createFont();
        style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
        style.setFont(font);
        XSSFCell cell;
        List<String> cellNames = new ArrayList<String>();
        cellNames.add("名称");
        cellNames.add("创建时间");
        for (int i = 0; i < cellNames.size(); i++) {
            cell = row.createCell(i);
            cell.setCellValue(cellNames.get(i));
            cell.setCellStyle(style);
        }
        row = sheet.createRow(sheet.getLastRowNum() + 1);
        for (int i = 0; i < dataList.size(); i++) {

            row.createCell(i).setCellValue(dataList.get(i));
        }
        OutputStream out = null;
        try {
            out = new FileOutputStream(filePath);
            wb.write(out);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }



    public static boolean appendToExcel(String excelPath, List<String> dataList) {
        try {
            FileInputStream fs = new FileInputStream(excelPath);//获取excel
            XSSFWorkbook wb = new XSSFWorkbook(fs);
            XSSFSheet sheet = wb.getSheetAt(0);//获取工作表
            FileOutputStream out = new FileOutputStream(excelPath);//向excel中添加数据
            Row row = sheet.createRow(sheet.getLastRowNum() + 1);
            for (int i = 0; i < dataList.size(); i++) {
                row.createCell(i).setCellValue(dataList.get(i));
            }
            out.flush();
            wb.write(out);
            wb.close();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
