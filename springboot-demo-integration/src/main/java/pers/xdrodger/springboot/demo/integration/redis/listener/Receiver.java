package pers.xdrodger.springboot.demo.integration.redis.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

/**
 * Created by xdrodger on 02/02/2018.
 */
@Component
public class Receiver {
    private static final Logger LOGGER = LoggerFactory.getLogger(Receiver.class);

    private CountDownLatch latch;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    public Receiver(CountDownLatch latch) {
        this.latch = latch;
    }

    public void receiveMessage(String message) {
        LOGGER.info("Received <" + message + ">");
        latch.countDown();
    }

    public void onMessage(Message message, byte[] pattern) {
        RedisSerializer<?> serializer = redisTemplate.getStringSerializer();
        String messageStr = (String) serializer.deserialize(message.getBody());
        String topic =  (String) serializer.deserialize(message.getChannel());
        System.out.println("messageStr：" + messageStr);
        System.out.println("topic：" + topic);
//        System.out.println("pattern：" + HexUtils.toHexString(pattern));
    }
}
