package pers.xdrodger.springboot.demo.service;

import pers.xdrodger.springboot.demo.entity.AccessLog;

/**
 * Created by xdrodger on 04/08/2018.
 */
public interface AccessLogService {
    AccessLog findById(Long id);
}
