package pers.xdrodger.springboot.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pers.xdrodger.springboot.demo.entity.AccessLog;
import pers.xdrodger.springboot.demo.repo.AccessLogRepo;
import pers.xdrodger.springboot.demo.service.AccessLogService;

/**
 * Created by xdrodger on 04/08/2018.
 */
@Service
public class AccessLogServiceImpl implements AccessLogService {
    @Autowired
    private AccessLogRepo accessLogRepo;

    @Override
    public AccessLog findById(Long id) {
        return accessLogRepo.findById(id);
    }
}
