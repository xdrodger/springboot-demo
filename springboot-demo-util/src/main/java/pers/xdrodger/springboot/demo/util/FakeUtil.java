package pers.xdrodger.springboot.demo.util;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by xiaolei on 2018/3/16.
 */
public class FakeUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(FakeUtil.class);

    private static final Properties properties = new OrderedProperties();

    private static InputStreamReader reader = null;

    public static String[] head_url;
    public static String[] first_name;
    public static String[] last_name;

    static {
        try {
            reader = new InputStreamReader(FakeUtil.class.getClassLoader().getResourceAsStream("fake.properties"), "UTF-8");
            properties.load(reader);
            head_url = properties.getProperty("fake.head.url").split(",");
            first_name = properties.getProperty("fake.first.name").split(",");
            last_name = properties.getProperty("fake.last.name").split(",");
        }catch (IOException e) {
            if (LOGGER.isInfoEnabled()){
                LOGGER.info("properties load failed",e);
            }
        }
    }

    public static List<String> getHeadUrl(int n) { // 要几个url就传几个
        Random random = new Random();
        Set<String> headUrls = new HashSet<String>();
        if (n >= head_url.length) {
            return Collections.emptyList();
        }
        while (true) {
            if (headUrls.size() >= n) {
                break;
            }
            headUrls.add(head_url[random.nextInt(head_url.length)]);
        }
        return new ArrayList<String>(headUrls);
    }

    public static List<String> getNickName(int n) {
        Set<String> nickNames = new HashSet<String>();
        while (true) {
            if (nickNames.size() >= n) {
                break;
            }
            Random random = new Random();
//            int seed = random.nextInt(5);
//            if (seed == 0) {
//                nickNames.add("x_" + RandomStringUtils.randomNumeric(3));
//            } else if (seed == 1) {
//                nickNames.add(RandomStringUtils.randomAlphabetic(3) + RandomStringUtils.randomNumeric(3));
//            } else if (seed == 2) {
//                nickNames.add("wt_" + RandomStringUtils.randomAlphanumeric(6));
//            } else {
//                nickNames.add(last_name[random.nextInt(last_name.length)] + first_name[random.nextInt(first_name.length)]);
//            }
            int seed = random.nextInt(6);
            if (seed == 0) {
                nickNames.add(first_name[random.nextInt(first_name.length)] + RandomStringUtils.randomNumeric(RandomUtils.nextInt(2, 5)));
            } else if (seed == 1) {
                nickNames.add(RandomStringUtils.randomAlphabetic(3) + RandomStringUtils.randomNumeric(RandomUtils.nextInt(2, 5)));
            } else if (seed == 2) {
                nickNames.add(last_name[random.nextInt(last_name.length)] + RandomStringUtils.randomAlphanumeric(RandomUtils.nextInt(2, 5)));
            } else if (seed == 3) {
                nickNames.add(RandomStringUtils.randomAlphabetic(RandomUtils.nextInt(4, 8)));
            } else if (seed == 4) {
                nickNames.add(RandomStringUtils.randomAlphanumeric(RandomUtils.nextInt(2, 9)));
            } else {
                nickNames.add(last_name[random.nextInt(last_name.length)] + first_name[random.nextInt(first_name.length)]);
            }
        }
        return new ArrayList<String>(nickNames);
    }

    /** Properties 读有序 **/
    static class OrderedProperties extends Properties {

        private static final long serialVersionUID = -4627607243846121965L;

        private final LinkedHashSet<Object> keys = new LinkedHashSet<>();

        public Enumeration<Object> keys() {
            return Collections.enumeration(keys);
        }

        public Object put(Object key, Object value) {
            keys.add(key);
            return super.put(key, value);
        }

        public Set<Object> keySet() {
            return keys;
        }

        public Set<String> stringPropertyNames() {
            Set<String> set = new LinkedHashSet<>();

            for (Object key : this.keys) {
                set.add((String) key);
            }

            return set;
        }
    }

    public static void main(String[] args) throws IOException{
        System.out.println(JSON.toJSONString(FakeUtil.getHeadUrl(3)));
        System.out.println(JSON.toJSONString(FakeUtil.getNickName(3)));

        System.out.println(JSON.toJSONString(FakeUtil.getHeadUrl(3)));
        System.out.println(JSON.toJSONString(FakeUtil.getNickName(3)));
    }
}
