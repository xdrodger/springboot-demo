package pers.xdrodger.springboot.demo.web;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by xdrodger on 04/08/2018.
 */
public class Test {

    public static  void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.get(Calendar.SECOND);
        calendar.add(Calendar.DAY_OF_MONTH, -7);
        System.out.println((calendar.getTimeInMillis() / 1000));
    }
}
