package pers.xdrodger.springboot.demo.web.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import pers.xdrodger.springboot.demo.integration.client.DemoClient;

import java.text.SimpleDateFormat;

/**
 * Created by xdrodger on 02/07/2018.
 */
@Component
public class MyApplicationRunner implements ApplicationRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(MyApplicationRunner.class);
    private SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");

    @Autowired
    private DemoClient demoClient;

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        LOGGER.info("程序启动后开始执行。。。");
    }
}
