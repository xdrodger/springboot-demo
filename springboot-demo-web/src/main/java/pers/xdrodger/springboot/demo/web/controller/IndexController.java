package pers.xdrodger.springboot.demo.web.controller;

/**
 * Created by xdrodger on 29/06/2018.
 */

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.RandomUtils;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import pers.xdrodger.springboot.demo.entity.AccessLog;
import pers.xdrodger.springboot.demo.integration.client.DemoClient;
import sun.rmi.runtime.Log;

import javax.net.ssl.SSLContext;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class IndexController {
    private static final Logger LOGGER = LoggerFactory.getLogger(IndexController.class);

    @Autowired
    private DemoClient demoClient;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private RestTemplate restTemplate;

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @RequestMapping(value = "/index/home")
    @ResponseBody
    public String home(@RequestBody Map<String, Object> params){
        Set<String> groupIds = new HashSet<>();
        try {
            TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
            SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
            SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
            CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();
            HttpComponentsClientHttpRequestFactory requestFactory =
                    new HttpComponentsClientHttpRequestFactory();
            requestFactory.setHttpClient(httpClient);
            restTemplate.setRequestFactory(requestFactory);
            HttpHeaders headers = new HttpHeaders();
            headers.add("User-Agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11Mozilla/5.0 (Windows NT 6.1) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11");
            headers.add("Cookie", "tt_webid=6594382265987204612; __tasessionId=sl1vr5p0s1535374268827; uuid=\"w:2a85e007adae41139bea26dd3f87939e\"; csrftoken=916725fc8691be9998b694c63b721563; login_flag=7c541ed80015b5a7cec9f82534d48f71; sessionid=797079157740aa878c79da2dca77dba8; sid_guard=\"797079157740aa878c79da2dca77dba8|1535374338|15552000|Sat\\054 23-Feb-2019 12:52:18 GMT\"; sid_tt=797079157740aa878c79da2dca77dba8; uid_tt=89bd45b736bd9d946d9f3f6906618819; WEATHER_CITY=%E5%8C%97%E4%BA%AC; tt_webid=6594382265987204612");
            headers.add("Authorization", "www.toutiao.com");

            HttpEntity<String> entity = new HttpEntity<String>( headers);
            long  time = 0;
            while (true) {
                try {
                    Thread.sleep(RandomUtils.nextInt(2000, 3000));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String url = "https://www.toutiao.com/c/user/favourite/?page_type=2&user_id=3916302824&max_behot_time=0&count=20&as=A1054BD8B38F7D0&cp=5B83EFC76D100E1&_signature=2-mNXhARgHIp4cwsg8wiNdvpjU&max_repin_time=";
                url += time;
                ResponseEntity<String> responseData = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
                LOGGER.info("response data={}", responseData.getBody());
                if (responseData.getStatusCode().is2xxSuccessful()) {
                    JSONObject respJson = JSON.parseObject(responseData.getBody());
                    time = respJson.getLong("max_repin_time");
                    LOGGER.info("max_repin_time={}" , sdf.format(time * 1000));
                    if (!respJson.getBoolean("has_more")) {
                        break;
                    }
                    for (Object o : respJson.getJSONArray("data")) {
//                        try {
//                            Thread.sleep(RandomUtils.nextInt(800, 1200));
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
                        JSONObject json = (JSONObject) o;
                        groupIds.add(json.getString("group_id") + "," + json.getString("item_id"));
//                        repin(restTemplate, json.getString("group_id"), json.getString("item_id"));
//                        unrepin(restTemplate, json.getString("group_id"), json.getString("item_id"));
                    }

                } else {
                    LOGGER.error("response data={}", responseData.getBody());
                    break;
                }
            }
            LOGGER.info("final data={}", groupIds.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
            SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
            SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
            CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();
            HttpComponentsClientHttpRequestFactory requestFactory =
                    new HttpComponentsClientHttpRequestFactory();
            requestFactory.setHttpClient(httpClient);
            restTemplate.setRequestFactory(requestFactory);
            HttpHeaders headers = new HttpHeaders();
            headers.add("User-Agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11Mozilla/5.0 (Windows NT 6.1) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11");
            headers.add("Cookie", "UM_distinctid=162a2bfba0fd30-09dc0f053591ab-336d7b05-1aeaa0-162a2bfba109b4;csrftoken=0f2da6a73f58c346daed0bfade72d3ac;tt_webid=6575402744462312974;tt_webid=6575402744462312974;uuid=\"w:16910e8dd92b41ae99759160c378c649\";ccid=f3d7ce72cf8da861097da3e4c1b72157;sso_uid_tt=5bbb4acbd76f3ca45031518d821a6897;toutiao_sso_user=467f8605d3dd29a4d86c91ea7639ccae;sso_login_status=1;__tasessionId=yysj8zsi21535380066804;CNZZDATA1259612802=2044644371-1529887454-%7C1535378054;login_flag=fa0fddfb58b2c335efea0a68bf363d6e;sessionid=8a23d79294a26bd31ccfd75fde0a1b5e;sid_tt=8a23d79294a26bd31ccfd75fde0a1b5e;uid_tt=bd22a318fe12963d79cf51eb2454e228;sid_guard=\"8a23d79294a26bd31ccfd75fde0a1b5e|1535380225|15552000|Sat\\054 23-Feb-2019 14:30:25 GMT\"");
            headers.add("Authorization", "www.toutiao.com");
            headers.add("Content-Type", "application/x-www-form-urlencoded");

            List<String> list = new ArrayList<>(groupIds);
            String url = "https://www.toutiao.com/group/repin/";
            for (int i = 0; i < list.size(); i ++) {
                try {
                    Thread.sleep(RandomUtils.nextInt(1500,2500));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                LOGGER.info("i = {}，str=", i, list.get(list.size() - 1 - i));
                String[] arr = list.get(list.size() - 1 - i).split(",");
                MultiValueMap<String, Object> postParameters = new LinkedMultiValueMap<>();
                postParameters.add("group_id", arr[0]);
                postParameters.add("item_id", arr[1]);
                HttpEntity<MultiValueMap<String, Object>> r = new HttpEntity<>(postParameters, headers);

                ResponseEntity<String> responseData = restTemplate.postForEntity(url, r, String.class);
                LOGGER.info("response data={}", responseData.getBody());
                if (responseData.getStatusCode().is2xxSuccessful()) {

                } else {
                    LOGGER.error("response data={}", responseData.getBody());
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return "success";
    }

    private void repin(RestTemplate restTemplate, String groupId, String itemId) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("User-Agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11Mozilla/5.0 (Windows NT 6.1) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11");
        headers.add("Cookie", "UM_distinctid=162a2bfba0fd30-09dc0f053591ab-336d7b05-1aeaa0-162a2bfba109b4;csrftoken=0f2da6a73f58c346daed0bfade72d3ac;tt_webid=6575402744462312974;tt_webid=6575402744462312974;uuid=\"w:16910e8dd92b41ae99759160c378c649\";ccid=f3d7ce72cf8da861097da3e4c1b72157;sso_uid_tt=5bbb4acbd76f3ca45031518d821a6897;toutiao_sso_user=467f8605d3dd29a4d86c91ea7639ccae;sso_login_status=1;__tasessionId=yysj8zsi21535380066804;CNZZDATA1259612802=2044644371-1529887454-%7C1535378054;login_flag=fa0fddfb58b2c335efea0a68bf363d6e;sessionid=8a23d79294a26bd31ccfd75fde0a1b5e;sid_tt=8a23d79294a26bd31ccfd75fde0a1b5e;uid_tt=bd22a318fe12963d79cf51eb2454e228;sid_guard=\"8a23d79294a26bd31ccfd75fde0a1b5e|1535380225|15552000|Sat\\054 23-Feb-2019 14:30:25 GMT\"");
        headers.add("Authorization", "www.toutiao.com");
        headers.add("Content-Type", "application/x-www-form-urlencoded");
        MultiValueMap<String, Object> postParameters = new LinkedMultiValueMap<>();
        postParameters.add("group_id", groupId);
        postParameters.add("item_id", itemId);
        HttpEntity<MultiValueMap<String, Object>> r = new HttpEntity<>(postParameters, headers);

        ResponseEntity<String> responseData = restTemplate.postForEntity("https://www.toutiao.com/group/repin/", r, String.class);
        LOGGER.info("response data={}", responseData.getBody());
    }

    private void unrepin(RestTemplate restTemplate, String groupId, String itemId) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("User-Agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11Mozilla/5.0 (Windows NT 6.1) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11");
        headers.add("Cookie", "UM_distinctid=162a2bfba0fd30-09dc0f053591ab-336d7b05-1aeaa0-162a2bfba109b4;csrftoken=0f2da6a73f58c346daed0bfade72d3ac;tt_webid=6575402744462312974;tt_webid=6575402744462312974;uuid=\"w:16910e8dd92b41ae99759160c378c649\";ccid=f3d7ce72cf8da861097da3e4c1b72157;sso_uid_tt=5bbb4acbd76f3ca45031518d821a6897;toutiao_sso_user=467f8605d3dd29a4d86c91ea7639ccae;sso_login_status=1;__tasessionId=yysj8zsi21535380066804;CNZZDATA1259612802=2044644371-1529887454-%7C1535378054;login_flag=fa0fddfb58b2c335efea0a68bf363d6e;sessionid=8a23d79294a26bd31ccfd75fde0a1b5e;sid_tt=8a23d79294a26bd31ccfd75fde0a1b5e;uid_tt=bd22a318fe12963d79cf51eb2454e228;sid_guard=\"8a23d79294a26bd31ccfd75fde0a1b5e|1535380225|15552000|Sat\\054 23-Feb-2019 14:30:25 GMT\"");
        headers.add("Authorization", "www.toutiao.com");
        headers.add("Content-Type", "application/x-www-form-urlencoded");
        HttpEntity<String> entity = new HttpEntity<String>( headers);
        String url = "https://www.toutiao.com/group/unrepin/";
        url += "?group_id=" + groupId;
        url += "&item_id=" + itemId;
        ResponseEntity<String> responseData = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        LOGGER.info("response data={}", responseData.getBody());
    }

    @RequestMapping(value = "/index/test/db")
    @ResponseBody
    public String testDb(@RequestBody Map<String, Object> params){
        AccessLog accessLog = demoClient.findById(1L);
        LOGGER.info(JSON.toJSONString(accessLog));
        return "success";
    }

    @RequestMapping(value = "/index/test/excel")
    @ResponseBody
    public String testExcel(@RequestBody Map<String, Object> params){
        LOGGER.info(JSON.toJSONString(demoClient.testExcel()));
        return "success";
    }

    @RequestMapping(value = "/index/test/redis")
    @ResponseBody
    public String testRedis(@RequestBody Map<String, Object> params){
        LOGGER.info(JSON.toJSONString(demoClient.testRedis()));
        return "success";
    }

    @RequestMapping(value = "/index/test/redis/msg")
    @ResponseBody
    public String send(@RequestBody Map<String, Object> params){
        LOGGER.info("Sending message...");
        redisTemplate.convertAndSend("chat", "Hello from Redis!");
        JSONObject syncAcctInfo = new JSONObject();
        syncAcctInfo.put("id", 123);
        syncAcctInfo.put("userId", 996);
        syncAcctInfo.put("productType", "pt041");
        redisTemplate.convertAndSend("syncAccountSucc", syncAcctInfo.toJSONString());
        redisTemplate.convertAndSend("userRegister", "15951569965 user register!");

        return "success";
    }
}
